import React from 'react';
import { createStackNavigator } from '@react-navigation/stack';
import { createDrawerNavigator } from '@react-navigation/drawer';
import SignIn from '../screens/unauthenticated/sign-in'
import Dashboard from '../screens/authenticated/dashboard'
import Verify from '../index'
import ProductDetail from '../screens/authenticated/product-detail'
const Drawer = createDrawerNavigator()
const Stack = createStackNavigator()

const RouterUnauthenticated = ({route}) =>{
    const drawer = () =>{
      return (
        <Drawer.Navigator
          initialRouteName={route}
          screenOptions={{
            gestureEnabled: false
          }}
        >
          <Drawer.Screen
            name="SignIn"
            component={SignIn}
            options={{
              drawerLockMode: 'locked-closed'
            }}
          />
          <Drawer.Screen
            name="Dashboard"
            component={Dashboard}
          />
        </Drawer.Navigator>
      )
    }
    return(
      <Stack.Navigator
        headerMode="none"
      >
        <Stack.Screen
          name="Drawer"
          component={drawer}
        />
        <Stack.Screen
          name="Verify"
          component={Verify}
        />
        <Stack.Screen
          name="ProductDetail"
          component={ProductDetail}
        />
      </Stack.Navigator>
    )
}

export {RouterUnauthenticated}
