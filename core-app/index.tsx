import React from 'react';
import {
  View,
  Text,
  StyleSheet,
  TextInput
} from 'react-native';
import IconFont from 'react-native-vector-icons/FontAwesome'
import {RouterUnauthenticated} from './router-navigation/router-navigation'
import AsyncStorage from '@react-native-community/async-storage';
export default class Index extends React.Component <{}> {
  constructor () {
    super()
    this.state={
      load: false,
      routeInitial: 'SignIn'
    }
    this.validateLogin()
  }

  validateLogin(){
    AsyncStorage.getItem("isLogged").then((isLogged)=>{
      if (isLogged) {
        this.setState({
          routeInitial: "Dashboard"
        })
      }
      this.setState({
        load: true
      })
    })
  }
  render() {
    const { load, routeInitial } = this.state
    if (!load) {
      return <View/>
    }
    return <RouterUnauthenticated route={routeInitial}/>
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
  },
  headerSignIn:{
    flex: 2,
    backgroundColor: 'rgb(129, 0, 156)',
    alignItems: 'center',
    paddingTop: 40
  },
  bodySigIn:{
    flex: 3,
    backgroundColor: 'rgb(255, 255, 255)'
  },
  formContainer:{
    position: 'absolute',
    height: '100%',
    backgroundColor: '#FFFFFF',
    elevation: 5,
    width: '86%',
    top: '-35%',
    left: '7%',
    borderBottomRightRadius: 40,
    borderTopLeftRadius: 40,
    borderWidth: 1,
    borderColor: 'rgb(129, 0, 156)',
    display: "flex",
    alignItems: "center",
  },
  inputContainerEmail:{
    paddingTop: 80,
  },
  inputContainer:{
    width:'100%',
    paddingTop: 30,
    paddingLeft: 18,
    paddingRight:18,
  },
  input:{
    backgroundColor: 'rgba(101, 101, 101, 0.1)',
    borderRadius: 10,
    borderWidth: 1,
    borderColor: 'rgba(77, 77, 77, 0.4)'
  },
  buttonContainer:{
    display: 'flex',
    alignItems: 'center',
    justifyContent:'center',
    flex: 1,
  },
  buttonSignIn:{
    height:40,
    width: 150,
    backgroundColor: 'rgb(129, 0, 156)',
    justifyContent: 'center',
    alignItems: 'center',
    borderTopLeftRadius: 20,
    borderBottomRightRadius: 20,
    flexDirection: 'row',
  },
  buttonSignInText:{
    color: '#ffffff',
    fontSize: 20,
    fontWeight: 'bold',
    paddingLeft: 10
  }
});
