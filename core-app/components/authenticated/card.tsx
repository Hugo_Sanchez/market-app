import React from 'react';
import {
  View,
  Text,
  StyleSheet,
  Image,
  TouchableWithoutFeedback,
  Dimensions,
} from 'react-native';
const {height, width} = Dimensions.get('window')
export default class Card extends React.Component <{}> {
  render() {
    let {navigation, route, item} = this.props
    let {id, desc, price, photo, SKU}=item
    return (
      <TouchableWithoutFeedback onPress={(evt) => navigation.push(route,{})}>
        <View style={styles.containerCard}>
          <View style={styles.containerImageCard}>
            <Image source={{uri: photo}}
            style={styles.imageCard}/>
            <View style={styles.canvasImageCard}>

            </View>
          </View>
          <View style={styles.containerTextCard}>
            <Text  style={styles.textCard} numberOfLines={1}>{desc}</Text>
            <Text style={[styles.textCard, {fontSize: 13, textAlign: 'right'}]}>SKU: {SKU}</Text>
            <Text style={[styles.textCard, {fontSize: 10, textAlign: 'right'}]}>Price: {price}</Text>
          </View>
        </View>
      </TouchableWithoutFeedback>
    )
  }
}

const styles = StyleSheet.create({
  containerCard:{
    width: width / 2 - 5,
    borderRadius: 10,
    backgroundColor: '#37343a',
    marginLeft: 3,
    marginVertical: 3
  },
  containerImageCard:{
    height: height / 4 - 30,
    width: width / 2 -5,
    borderRadius: 10,
    paddingVertical: 2,
    paddingHorizontal: 2,
  },
  imageCard:{
    height: '100%',
    width: '100%',
    borderTopLeftRadius: 8,
    borderTopRightRadius: 8,
    right: 0,
    left: 0,
    top: 0,
    bottom: 0
  },
  canvasImageCard:{
    position: 'absolute',
    height: '100%',
    width: '100%',
    borderTopLeftRadius: 8,
    borderTopRightRadius: 8,
    backgroundColor: 'rgba(255,255,255, 0.2)',
    marginVertical: 2,
    marginHorizontal: 2,
  },
  containerTextCard:{
    paddingHorizontal: 10,
    paddingVertical: 5
  },
  textCard:{
    color: '#FFF',
    fontWeight: 'bold',
    fontSize: 14
  }
});
