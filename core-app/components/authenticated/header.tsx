import React from 'react';
import {
  View,
  Text,
  StyleSheet,
  TextInput,
  TouchableOpacity
} from 'react-native';
import IconFont from 'react-native-vector-icons/FontAwesome'
import AsyncStorage from '@react-native-community/async-storage'
import { StackActions } from '@react-navigation/native';
export default class MyComponent extends React.Component <{}> {
  constructor(){
    super()
    this.state={
      search: ""
    }
  }
  signOut(navigation){
    AsyncStorage.setItem("isLogged", "")
    navigation.dispatch(
      StackActions.replace('Verify', {})
    );
  }
  render() {
    let {navigation} = this.props
    return (
      <View style={styles.container}>
        {
          this.props.back?
          <View style={{
            flex: 1,
            justifyContent: 'center',
            paddingLeft: 10
          }}>
            <TouchableOpacity onPress={() => navigation.goBack()}>
              <View>
                <IconFont name="arrow-left" size={30} color="#ffffff" />
              </View>
            </TouchableOpacity>
          </View>
          :
          <View style={{flex: 5, flexDirection: 'row'}}>
            <View style={{flex: 6, justifyContent: 'center', paddingLeft: 20, paddingRight: 20, paddingVertical: 5, }}>
              <TextInput style={{
                height: 45,
                borderRadius: 10,
                backgroundColor: "rgba(255, 255, 255, 0.35)",
                fontSize: 18,
                paddingLeft:10,
              }} placeholder="Search" placeholderTextColor="rgba(255, 255, 255, 0.2)" onChangeText={(search)=>this.setState({search: search})}/>
            </View>
            <View style={{
              flex: 1,
              justifyContent: 'center',
            }}>
              <TouchableOpacity onPress={() => this.signOut(navigation)}>
                <View>
                  <IconFont name="sign-out" size={30} color="#ffffff" />
                </View>
              </TouchableOpacity>
            </View>
          </View>
        }
      </View>
    )
  }
}

const styles = StyleSheet.create({
  container: {
    minHeight: "8%",
    width: '100%',
    backgroundColor: 'rgb(129, 0, 156)',
    justifyContent: "center",
    flexDirection: 'row'
  },
});
