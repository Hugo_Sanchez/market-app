import React from 'react';
import {
  View,
  Text,
  StyleSheet,
  FlatList
} from 'react-native';
import Header from '../../components/authenticated/header'
import Card from '../../components/authenticated/card'
export default class Dashboard extends React.Component <{}> {
  constructor(){
    super()
    this.state={
      DATA: []
    }
    this.onFetchInfo()
  }
  onFetchInfo(){
    fetch(`https://e-voluta.com/angularEx1/PLP.html`).then((response)=>response.json())
    .then((responseJSON)=>{
      if (responseJSON.products) {
        this.setState({
          DATA: responseJSON.products
        })
      }
    })
  }
  renderCard(item){
    return <Card item={item} navigation={this.props.navigation} route="ProductDetail"/>
  }
  render() {
    let { DATA } = this.state
    return (
      <View style={styles.container}>
        <Header navigation={this.props.navigation}/>
        <FlatList
          data={DATA}
          renderItem={({item})=>this.renderCard(item)}
          keyExtractor={(x, i) => i}
          numColumns={2}
        />
      </View>
    )
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
  },
});
