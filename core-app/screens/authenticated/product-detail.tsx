import React from 'react';
import {
  View,
  Text,
  StyleSheet,
  Image
} from 'react-native';
import Header from '../../components/authenticated/header'
export default class ProductDetail extends React.Component <{}> {
  constructor(){
    super()
    this.state={
      product: {}
    }
    this.onFetchProduct()
  }
  onFetchProduct(){
    fetch(`https://e-voluta.com/angularEx1/PDP.html`).then(response=>response.json())
    .then(responseJSON=>{
      if (responseJSON.id) {
        this.setState({
          product: responseJSON
        })
      }
    })
  }
  render() {
    let {SKU, photo, title, desc, unit, price} = this.state.product
    return (
      <View style={styles.container}>
        <Header back={true} navigation = {this.props.navigation}/>
        <View style={{
          height: "30%",
          width: "100%"
        }}>
          <Image source={{uri: photo}}
          style={{
            height: "100%",
            width: "100%",
          }}/>
          <View style={{
            height: 45,
            backgroundColor: 'rgba(0, 0, 0, 0.7)',
            position: 'absolute',
            width: '100%',
            bottom: 0,
            paddingLeft: 5,
            justifyContent: "center"
          }}>
            <Text style={styles.textName}>{title}</Text>
          </View>
          <View>
            <View style={styles.containerTitle}>
              <Text style={styles.textTitle}>
                SKU:
              </Text>
              <Text style={styles.textDesc}>
                {SKU} {unit}
              </Text>
            </View>

            <View style={styles.containerTitle}>
              <Text style={styles.textTitle}>
                Price:
              </Text>
              <Text style={styles.textDesc}>
                {price}
              </Text>
            </View>
            <View style={{paddingHorizontal: 10}}>
              <Text style={{fontSize: 18}}>{desc}</Text>
            </View>
          </View>
        </View>
      </View>
    )
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
  },
  textName:{color: '#ffffff', fontWeight: 'bold', fontSize: 20},
  containerTitle:{flexDirection: 'row', paddingHorizontal: 10, paddingVertical: 10},
  textTitle:{fontWeight: 'bold', fontSize: 20},
  textDesc:{ paddingLeft: 10, fontSize: 20}
});
