import React from 'react';
import {
  View,
  Text,
  StyleSheet,
  TextInput,
  TouchableOpacity
} from 'react-native'
import IconFont from 'react-native-vector-icons/FontAwesome'
import AsyncStorage from '@react-native-community/async-storage'
import { StackActions } from '@react-navigation/native';
export default class SignIn extends React.Component <{}> {
  constructor(props:any){
    super(props)
    this.state={
      email: "",
      password: ""
    }
    AsyncStorage.setItem("email", "example@example.com")
    AsyncStorage.setItem("password", "Market123")
  }
  onSignIn(){
    let {email, password} = this.state
    AsyncStorage.multiGet(["password", "email"],(error, store)=>{
      if (email == store[1][1] && password == store[0][1]) {
        AsyncStorage.setItem("isLogged", "Ok")
          this.props.navigation.dispatch(
            StackActions.replace('Verify', {})
          );
      }else{
        alert("Ingrese los datos correctamente")
      }
    })
  }
  render() {
    return (
      <View style={styles.container}>
        <View style={styles.headerSignIn}>
          <IconFont name="shopping-basket" size={80} color="#ffffff" />
        </View>
        <View style={styles.bodySigIn}>
          <View style={[styles.formContainer, styles.inputContainerEmail]}>
            <View style={styles.inputContainer}>
              <TextInput
                style={styles.input}
                placeholder="Please enter email or username"
                onChangeText={(email) => this.setState({email: email})}
              />
            </View>
            <View style={styles.inputContainer}>
              <TextInput
                autoCompleteType="password"
                placeholder="Please enter your password"
                style={styles.input}
                secureTextEntry={true}
                onChangeText={(password) => this.setState({password: password})}
              />
            </View>
            <View style={styles.buttonContainer}>
              <TouchableOpacity
                onPress={()=>this.onSignIn()}
              >
                <View style={styles.buttonSignIn}>
                  <IconFont name="sign-in" size={20} color="#ffffff" />
                  <Text style={styles.buttonSignInText}>
                    Sign In
                  </Text>
                </View>
              </TouchableOpacity>
            </View>
          </View>
        </View>
      </View>
    )
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
  },
  headerSignIn:{
    flex: 2,
    backgroundColor: 'rgb(129, 0, 156)',
    alignItems: 'center',
    paddingTop: 40
  },
  bodySigIn:{
    flex: 3,
    backgroundColor: 'rgb(255, 255, 255)'
  },
  formContainer:{
    position: 'absolute',
    height: '100%',
    backgroundColor: '#FFFFFF',
    elevation: 5,
    width: '86%',
    top: '-35%',
    left: '7%',
    borderBottomRightRadius: 40,
    borderTopLeftRadius: 40,
    borderWidth: 1,
    borderColor: 'rgb(129, 0, 156)',
    display: "flex",
    alignItems: "center",
  },
  inputContainerEmail:{
    paddingTop: 80,
  },
  inputContainer:{
    width:'100%',
    paddingTop: 30,
    paddingLeft: 18,
    paddingRight:18,
  },
  input:{
    backgroundColor: 'rgba(101, 101, 101, 0.1)',
    borderRadius: 10,
    borderWidth: 1,
    borderColor: 'rgba(77, 77, 77, 0.4)'
  },
  buttonContainer:{
    display: 'flex',
    alignItems: 'center',
    justifyContent:'center',
    flex: 1,
  },
  buttonSignIn:{
    height:40,
    width: 150,
    backgroundColor: 'rgb(129, 0, 156)',
    justifyContent: 'center',
    alignItems: 'center',
    borderTopLeftRadius: 20,
    borderBottomRightRadius: 20,
    flexDirection: 'row',
  },
  buttonSignInText:{
    color: '#ffffff',
    fontSize: 20,
    fontWeight: 'bold',
    paddingLeft: 10
  }
});
